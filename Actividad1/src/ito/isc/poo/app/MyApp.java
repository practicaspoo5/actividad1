package ito.isc.poo.app;
import java.util.Scanner;

public class MyApp {
	public static Scanner tec = new Scanner(System.in);
//*******************************************************************************************
	public static int[] llenar() {
		int lin[] = new int[8];
		
		System.out.println("Inserte la cantidad que empaqueto cada linea");
		
		for(int i=0; i<8; i++) {
			System.out.print("Linea "+(i+1)+": ");
			lin[i]=tec.nextInt();
		}
		
		return lin;
	}
//*****************************************************************************************
	public static int prom(int lin[]) {
		int r=0;
		
		for(int i=0; i<lin.length; i++) {
			r=r+lin[i];
		}
		
		r=r/lin.length;
		return r;
	}
//*******************************************************************************************
	public static int mayor(int lin[]) {
		int a=0, b=0;
		
		for(int i=0; i<lin.length; i++) {
			
			if(a<lin[i]) {
				b=i+1;
				a=lin[i];
			}
		}
		
		return b;
		
	}
//*********************************************************************************************
	public static int menor(int lin[]) {
		int a=lin[0], b=0;
		
		for(int i=0; i<lin.length; i++) {
			if(lin[i]!=0) {
				if(a>lin[i]) {
					b=i+1;
					a=lin[i];
				}
			}
		}
		
		return b;
	}
//*********************************************************************************************
	public static void menu() {
		int lin[] = new int[8];
		int r=0;
		lin=llenar();
		
		System.out.println("El promedio de todas las lineas es: "+prom(lin));
		System.out.println("La linea que mas productos empaqueto fue la linea: "+mayor(lin));
		r = menor(lin);
		lin[r-1]=0;
		System.out.println("Las lineas que menos productos empaquetaron fueron la "+r+" y la "+menor(lin));
		
	}
//*********************************************************************************************	
	public static void main(String[] args) {
		menu();
	}

}
